.. _media_synchronization:

#####################
Media Synchronization
#####################

With VLC for iOS, you can watch, add or download media files using various synchronization methods. This section highlights the different methods supported by VLC for iOS and how to use them.

*******************
iTunes file sharing
*******************

You can use the :guilabel:`File Sharing` feature on iTunes to add or delete media files to VLC for iOS. Read more about how to implement this on `Apple's documentation <http://support.apple.com/kb/HT4094>`__.

**************
Cloud services
**************

In VLC for iOS, you can upload, stream, and download media files with any of the following cloud service providers: `Dropbox  <https://www.dropbox.com/>`__, `Google Drive  <https://www.google.com/drive/>`__, 
`Box  <https://account.box.com>`__, `OneDrive  <https://www.microsoft.com/en/microsoft-365/onedrive/online-cloud-storage>`__, `iCloud Drive <https://www.icloud.com/>`__.

To do this, follow the steps below:

1. Tap the :guilabel:`Network` tab on your VLC for iOS application.
2. Tap on :guilabel:`Cloud Services`.

.. figure::  /images/gettingstarted/mediasynchronization/cloud_services.jpeg
   :align:   center
   :width: 40%

3. Select and log in to your preferred cloud service platform. 

.. figure::  /images/gettingstarted/mediasynchronization/cloud_services_main.jpeg
   :align:   center
   :width: 40%

4. Start streaming and downloading media files directly from the cloud service. 


*********************
Download media files
*********************

You can also download media files directly from the web with VLC for iOS. To do this, tab on :guilabel:`Network`, select :guilabel:`Downloads`, 
enter the URL address of the file you want to download and tap on the :guilabel:`Download` button. A progress bar will indicate when the download is complete.

.. container:: tocdescr

   .. container:: descr

      .. figure:: /images/gettingstarted/mediasynchronization/downloads.jpg

   .. container:: descr

      .. figure:: /images/gettingstarted/mediasynchronization/downloads_main.jpeg


.. note:: Downloads from popular video playback sites such as YouTube or Vimeo are not supported in VLC for iOS.

***************
Share via Wi-Fi
***************

If your iOS device and your computer are on the same local WiFi network, you can use the :guilabel:`Sharing via WiFi` media synchronization method to add media files to VLC for iOS.
To do this, follow the steps below:

1. Ensure that your computer and iOS device are on the same network.
2. Tap the :guilabel:`Network` tab on your VLC for iOS application.

.. figure::  /images/gettingstarted/mediasynchronization/network_tab.jpg
   :align:   center
   :width: 40%

3. Tap the :guilabel:`Sharing via WiFi` toggle button to activate Wifi sharing.

.. figure::  /images/gettingstarted/mediasynchronization/sharing_via_wifi.jpg
   :align:   center
   :width: 40%

4. Enter the IP address on your computer's web browser to access the :guilabel:`Sharing via WiFi` page on the computer’s browser window.

.. figure::  /images/gettingstarted/mediasynchronization/wifi_upload.png
   :align:   center

.. note:: If your VLC app is not open at this point, you won't be able to access the :guilabel:`Sharing via WiFi` page on the computer’s browser window.

5. Drag and drop files in the browser window or click on the :guilabel:`+` button to use the file picker dialogue to upload the media file(s) to your VLC for iOS mobile application. 


VLC for iOS WiFi upload supports multiple uploads simultaneously and will indicate through a progress bar when the upload is complete. 
You can start the playback of any file on your iOS device as soon as they appear without waiting for the media file to finish uploading.

*******************************************
Open a media file without copying it to VLC
*******************************************

In addition to using the aforementioned media synchronization methods, you can also open a file without copying it to VLC. Here's how:

1. Open your VLC for iOS application and tap the :guilabel:`Network` tab.
2. Tap on :guilabel:`Local files` to access the media files on your device.

.. figure::  /images/gettingstarted/mediasynchronization/local_files.jpg
   :align:   center
   :width: 40%

3. Tap on the media file you want to open directly on VLC.

.. figure::  /images/gettingstarted/mediasynchronization/local_files_home.jpg
   :align:   center
   :width: 40%

4. If you followed the steps above, your media file should start playing on VLC. Enjoy!

.. figure::  /images/gettingstarted/mediasynchronization/opened_file.jpg
   :align:   center
   :width: 40%