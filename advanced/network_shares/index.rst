##############
Network Shares
##############

A network share is a computer resource made available from one host to other hosts on a computer network. 

Suppose VLC is connected to a network share of your computer. In that case, it can remotely access different media resources from that computer transparently as if it were a media resource on VLC. This means that you can consume media 
through local network shares using your VLC for iOS application.

Here's a list of file server protocols supported by VLC for iOS:

* :doc:`/advanced/network_shares/smb`
* :doc:`/advanced/network_shares/ftp`
* :doc:`/advanced/network_shares/plex`

.. toctree::
   :maxdepth: 2
   :hidden:

   smb.rst
   ftp.rst
   plex.rst
