.. _settings:

########
Settings
########

VLC can be customized or modified to meet your preference from the settings. This documentation covers the different sections of VLC for iOS that can be changed/updated via VLC's Settings.

**************************************
How to change the theme of VLC for iOS
**************************************

You can change the appearance of VLC for iOS to either a bright or dark theme. To do this, follow the steps listed below;

1. Tap on :guilabel:`Settings`.
2. Click on :guilabel:`Appearance`. 
3. Select your preferred theme.

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/theme_appearance.jpeg


********************************
How to control playback gestures 
********************************

On VLC for iOS, playback gestures are enabled by default. Follow the steps below to turn it off;


1. Tap on :guilabel:`Settings`.
2. Scroll down to the **Control playback with gestures** section. 
3. Disable the gestures. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/disable_gestures.jpeg


*********
Subtitles
*********

You can change the font, font size, color, and text encoding format of subtitles on VLC for iOS. Find a breakdown of how to achieve this below;

How to change the font of subtitles
-----------------------------------

To change the font of subtitles on VLC for iOS, tap on :guilabel:`Settings`, scroll down to the **Subtitles** section, tap on :guilabel:`Font` and select your preferred font type. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/font_type.jpeg


How to change the font size of subtitles
----------------------------------------

To change the font size of subtitles on VLC for iOS, tap on :guilabel:`Settings`, scroll down to the **Subtitles** section, tap on :guilabel:`Relative font-size` and select your preferred font size. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/font_size.jpeg

How to make subtitles bold
--------------------------

To make the subtitles on a playback bold, tap on :guilabel:`Settings`, scroll down to the **Subtitles** section, and enable :guilabel:`Use Bold font`.

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/bold_subtitles.jpeg

How to change the font color of subtitles
-----------------------------------------

To change the font color of subtitles on VLC for iOS, tap on :guilabel:`Settings`, scroll down to the **Subtitles** section, tap on :guilabel:`Font color` and select your preferred color. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/font_color.jpeg


How to change the text encoding format of subtitles
---------------------------------------------------

To change the text encoding format of subtitles on VLC for iOS, tap on :guilabel:`Settings`, scroll down to the **Subtitles** section, tap on :guilabel:`Text Encoding` and select your preferred format.  

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/text_encoding.jpeg

*****
Video
*****

This section covers how to deblock videos, activate/deactivate deinterlacing, and enable/disable hardware accelerated decoding on VLC for iOS.

How to deblock videos
---------------------

Deblocking is a type of post-processing that makes a video appear less pixellated by rearranging pixels and hues to emulate contours of curves.

Follow the steps below to deblock videos on VLC for iOS.

1. Open VLC for iOS and tap on :guilabel:`Settings` at the bottom-right corner of your screen. 
2. Scroll down to **Video** and tap on :guilabel:`Deblocking filter`. 
3. Select your preferred deblocking filter. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/deblocking_filter.jpeg

How to activate or deactivate deinterlacing
-------------------------------------------

Deinterlacing is the process of converting videos that contains alternating half-pictures to a full picture at a time. 

Follow the steps below to activate or deactivate deinterlacing on VLC for iOS.

1. Open VLC for iOS and tap on :guilabel:`Settings` at the bottom-right corner of your screen. 
2. Scroll down to **Video** and tap on :guilabel:`Deinterlace`. 
3. Select your preferred option between; **On**, **Off**, and **Automatic**. 

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/deinterlace.jpeg

How to enable or disable hardware accelerated decoding 
-------------------------------------------------------

Hardware acceleration decoding is the process of speeding up a phone's processing power by moving the workload to a part that can do the job faster. In VLC, hardware accelerated decoding generally refers to using your graphics card for better performance. 

Follow the steps below to enable or disable hardware accelerated decoding on VLC for iOS.

1. Open VLC for iOS and tap on :guilabel:`Settings` at the bottom-right corner of your screen. 
2. Scroll down to **Video** and tap on :guilabel:`Hardware Decoding`. 
3. To enable, tap **On**.
4. To disable, tap **Off**.

.. container:: tocdescr

   .. container:: descr

      .. figure::  /images/basic/playbackgestures/settings.jpeg

   .. container:: descr

      .. figure::  /images/basic/settings/hardware_decoding.jpeg